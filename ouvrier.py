from categorie import Categorie
from ressources import Ressource

class Ouvrier:
    def __init__(self, nom: str, lesRessources: dict):
        self.setNom(nom)
        self.setLesRessources(lesRessources)

    def getNom(self) -> str:
        """Getter for nom from Ouvrier class"""
        return self.__nom

    def setNom(self, unNom: str):
        """Setter for nom"""
        self.__nom = unNom

    def getLesRessources(self) -> dict:
        """Getter for lesRessources from Ouvrier class"""
        return self.__lesRessources

    def setLesRessources(self, dicoRessources: dict):
        """Setter for lesRessources"""
        self.__lesRessources = dicoRessources

    def getQuantiteByRessource(self, ressource: Ressource) -> int:
        res = 0

        if ressource in self.__lesRessources.keys():
            res = self.__lesRessources[ressource]
        return res

    def cout(self):

        return 0

    def __str__(self):

        aff = ""
        affcout = ""
        for elem in self.__lesRessources:
            if self.__lesRessources[elem] != 0:
                aff += "Ressource produite: "+ str(elem) + " - Quantité: " + str(self.__lesRessources[elem]) + "\n"

        if self.cout() == 0:
            affcout = "gratuit"
        else:
            affcout = str(self.cout())

        return "Ouvrier: "+self.__nom+ "\n" + aff +"Cout de mise en chantier: " + affcout + "\n"





