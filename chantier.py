from batiment import Batiment
from ouvrier import Ouvrier
from ressources import Ressource

class Chantier:

    def __init__(self, unBatiment: Batiment):
        self.setBatiment(unBatiment)
        self.__estTermine = False
        self.lesOuvriers = []
        self.lesMachines = []

    def getBatiment(self) -> Batiment:
        """Getter for unBatiment from Chantier class"""
        return self.__unBatiment

    def setBatiment(self, unBatiment: Batiment) -> None:
        """Setter for unBatiment"""
        self.__unBatiment = unBatiment

    def envoyerTravaillerOuvrier(self, unOuvrier: Ouvrier) -> bool:
        self.lesOuvriers.append(unOuvrier)
        ok = True
        for uneRessource in self.__unBatiment.getConstruRess().keys():
            if self.__unBatiment.getQuantiteByRessource(uneRessource) > self.quantiteEquipeByRessource(uneRessource):
                ok = False
        return ok


    def retirerOuvrier(self, unOuvrier: Ouvrier):
        self.lesOuvriers.remove(unOuvrier)

    def estTermine(self) -> bool:

        return self.__estTermine

    def quantiteEquipeByRessource(self, uneRessource: Ressource) -> int:
        totRessource = 0

        for unOuvrier in self.lesOuvriers:
            nombreRessource = unOuvrier.getQuantiteByRessource(uneRessource)
            totRessource = totRessource + nombreRessource
        return totRessource

    def __str__(self):

        return self.__unBatiment.getNomBat() + " est en construction"