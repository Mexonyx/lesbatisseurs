import unittest
from ressources import Ressource
from ouvrier import Ouvrier
from categorie import Categorie
from affranchi import Affranchi


class testOuvrierCout(unittest.TestCase):

    def setUp(self) -> None:

        self.resBois = Ressource("bois")
        self.resPierre = Ressource("pierre")
        self.dico = {self.resBois: 1, self.resPierre: 1}

        self.apprenti = Categorie("apprenti", 2)

        self.affranchi = Affranchi("Michel", self.apprenti, self.dico)

        self.app1 = Ouvrier("Bonjour", self.dico)

    def testOuvrierCout(self):
        self.assertEqual(self.app1.cout(),0,"Erreur cout simple Ouvrier à zéro")

    def testCategCout(self):
        self.assertEqual(self.affranchi.cout(), 2, "Erreur cout d'un apprenti n'est pas égale a deux")


if __name__ == '__main__':
    unittest.main()
