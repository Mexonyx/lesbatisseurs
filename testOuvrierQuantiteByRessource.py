import unittest
from ressources import Ressource
from ouvrier import Ouvrier


class testOuvrierQuantiteByRessource(unittest.TestCase):

    def setUp(self) -> None:

        self.resBois = Ressource("bois")
        self.resDecoration = Ressource("decoration")
        self.resPierre = Ressource("pierre")
        self.resArchitecture = Ressource("architecture")

        self.app1 = Ouvrier("Ahmès", {self.resBois: 1, self.resPierre: 1})

    def testOuvrierQuantiteByRessourceProduite(self):
        self.assertEqual(self.app1.getQuantiteByRessource(self.resBois), 1, "Erreur quantite sur ressource produite")

    def testOuvrierQuantiteByRessourceNonProduite(self):
        self.assertEqual(self.app1.getQuantiteByRessource(self.resArchitecture), 0, "Erreur quantite sur ressource non produite")


if __name__ == '__main__':
    unittest.main()
