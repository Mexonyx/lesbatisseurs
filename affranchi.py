from ouvrier import Ouvrier
from categorie import Categorie
from outil import Outil

class Affranchi(Ouvrier):

    def __init__(self, nom: str, categorie: Categorie, lesRessources: dict):

        super().__init__(nom, lesRessources)
        self.setCategorie(categorie)
        self.outil = None

    def getCategorie(self) -> Categorie:
        """Getter for uneCateg from Affranchi class"""
        return self.__uneCateg

    def setCategorie(self, categ: Categorie):
        """Setter for uneCateg"""
        self.__uneCateg = categ

    def cout(self) -> int:

        return self.__uneCateg.getSalaire()

    def equiper(self, unOutil: Outil):

        self.outil = unOutil

    def __str__(self):

        outil = ""

        if self.outil != None:
            outil = "Ressource produite par un outil (" + self.outil.getLibelle()+"): " + str(self.outil.getRessource()) +" - Quantité: " + str(self.outil.getQuantite()) + "\n"


        return super().__str__() + outil + "Catégorie: " + self.__uneCateg.getLibelle()