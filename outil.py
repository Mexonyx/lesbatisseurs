from ressources import Ressource

class Outil:

    def __init__(self, libelle: str, quantite: int, prix: int, uneRessource: Ressource):
        self.setLibelle(libelle)
        self.setQuantite(quantite)
        self.setPrix(prix)
        self.setRessource(uneRessource)

    def getLibelle(self) -> str:
        """Getter for libelle from Outil class"""
        return self.__libelle

    def setLibelle(self, libelle: str):
        """Setter for libelle"""
        self.__libelle = libelle

    def getQuantite(self) -> int:
        """Getter for quantite from Outil class"""
        return self.__quantite

    def setQuantite(self, value: int):
        """Setter for quantite"""
        self.__quantite = value

    def getPrix(self) -> int:
        """Getter for prix from Outil class"""
        return self.__prix

    def setPrix(self, value: int):
        """Setter for prix"""
        self.__prix = value

    def getRessource(self) -> Ressource:
        """Getter for uneRessource from Outil class"""
        return self.__uneRessource

    def setRessource(self, value: Ressource):
        """Setter for uneRessource"""
        self.__uneRessource = value