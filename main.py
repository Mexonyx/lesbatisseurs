from ouvrier import Ouvrier
from categorie import Categorie
from ressources import Ressource
from batiment import Batiment
from affranchi import Affranchi
from outil import Outil
from chantier import Chantier

#Les roussources
lesRessources = [Ressource("Bois"),
                 Ressource("Pierre"),
                 Ressource("Architecture"),
                 Ressource("Décoration")]
#----------------------------


#dictionnaire des ressources pour un ouvrier ou un batiment
dicRessources = {lesRessources[0]: 0, lesRessources[1]: 2, lesRessources[2]: 3, lesRessources[3]: 1}
dicRessources2 = {lesRessources[0]: 5, lesRessources[1]: 3, lesRessources[2]: 1, lesRessources[3]: 3}

dicBatRessource = {lesRessources[0]: 5, lesRessources[1]: 3, lesRessources[2]: 1, lesRessources[3]: 3}
#----------------------------

#--------creer outil------

unoutil = Outil("scie", 2, 3, lesRessources[0])

#-------------------------


#creer les catégories
categUne = Categorie("Apprenti", 2)
categDeux = Categorie("Maitre", 5)
#----------------------------


#creer les ouvriers
unfefe = Affranchi("fefe",categUne, dicRessources)
unmax = Affranchi("max", categDeux, dicRessources2)
#----------------------------


#creer les batiments
unBatiment = Batiment("Le phare d'Alexandrie", 5, 14, dicBatRessource)
#----------------------------

unfefe.equiper(unoutil)

unChantier = Chantier(unBatiment)

print(unfefe, "\n")
print(unmax, "\n")
print(unChantier)

