from batiment import Batiment
from ressources import Ressource

class Machine(Batiment):

    def __init__(self, nomMachine: str, victoryPoint: int, renduMonaie: int, lesRessources: Ressource):
        super().__init__(nomMachine, victoryPoint, renduMonaie, lesRessources)