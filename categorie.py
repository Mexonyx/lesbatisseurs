class Categorie:

    def __init__(self, libelle: str, salaire: int):
        self.setLibelle(libelle)
        self.setSalaire(salaire)

    def getLibelle(self) -> str:
        """
        Getter le type d'ouvrier que c'est
        :return: le type d'ouvrier
        """
        return self.__libelle

    def setLibelle(self, value: str) -> None:
        """
        setter le type d'ouvrier
        :param value: string
        """

        self.__libelle = value

    def setSalaire(self, value: int) -> None:
        """
        setter le salaire par type d'ouvrier
        :param value: int
        """

        self.__salaire = value

    def getSalaire(self) -> int:
        """
        Guetter le salaire pour une categorie d'ouvrier
        :return: lre salaire du type d'ouvrier
        """

        return self.__salaire



    def __str__(self):

        return self.__libelle