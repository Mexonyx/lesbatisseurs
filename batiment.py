from ressources import Ressource

class Batiment:

    def __init__(self, nomBat: str, vitoryPoint: int, moneyRend: int, construRess: dict):

        self.setNomBat(nomBat)
        self.setVictoryPoint(vitoryPoint)
        self.setMoneyRand(moneyRend)
        self.setConstruRess(construRess)

    def getNomBat(self) -> str:
        """
        getter le nom du batiment
        :return: le nom du batiment en question
        """

        return self.__nomBat

    def setNomBat(self, value: str) -> None:
        """
        setter le nom du batiment
        :param value: nom du batiment
        """

        self.__nomBat = value

    def getVictoryPoint(self) -> int:
        """
        getter les points de victoires d'un batiment
        :return: les points de victoires d'un batiment
        """

        return self.__victoryPoint

    def setVictoryPoint(self, value: int) -> None:
        """
        setter les points de victoire d'un batiment
        :param value: nombre de points de victoire d'un batiment
        """

        self.__victoryPoint = value

    def getMoneyRand(self) -> int:
        """
        gettuer la monnaie redonner par un batiment
        :return: la monnaie redonner au joueur une fois le batiment fini de construire
        """

        return self.__moneyRand

    def setMoneyRand(self, value: int) -> None:
        """
        setter la monnaie rendu par un batiment
        :param value: monnaie rendu
        """

        self.__moneyRand = value

    def getConstruRess(self) -> dict:
        """
        getter les ressources d'un batiment
        :return: les ressources du batiment
        """

        return self.__construRess

    def setConstruRess(self, value: dict):
        """
        setter les ressources du batiment
        :param value: dictionnaire de ressource
        """
        self.__construRess = value

    def getQuantiteByRessource(self, ressource: Ressource) -> int:
        res = 0

        if ressource in self.__construRess.keys():
            res = self.__construRess[ressource]
        return res

    def __str__(self):
        aff = ""
        for elem in self.__construRess:
            aff += str(elem) + " :" + str(self.__construRess[elem]) + " "

        return self.__nomBat + " donne " + str(self.__moneyRand) + " sesterces " + "et " + str(self.__victoryPoint) + " points de victoires. Pour le construire il faut: " + aff