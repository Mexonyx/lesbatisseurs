import unittest

from affranchi import Affranchi
from chantier import Chantier
from machine import Machine
from outil import Outil
from ressources import Ressource
from categorie import Categorie
from ouvrier import Ouvrier
from batiment import Batiment


class testChantierEnvoyerTravaillerOuvrier(unittest.TestCase):

    def setUp(self) -> None:

        self.catManoeuvre = Categorie("Manoeuvre",3)
        self.catApprenti = Categorie("Apprenti",2)
        self.catMaitre = Categorie("Maitre",5)
        self.catCompagnon = Categorie("Compagnon",4)

        self.resBois = Ressource("bois")
        self.resDecoration = Ressource("decoration")
        self.resPierre = Ressource("pierre")
        self.resArchitecture = Ressource("architecture")

        self.mano1 = Affranchi("Hamosis",self.catManoeuvre,{self.resBois:1 , self.resDecoration: 2})
        self.compa1 = Affranchi("Djersis",self.catCompagnon,{self.resPierre: 1,self.resArchitecture: 3})

        self.ouv1 = Ouvrier("Ahmès", {self.resPierre: 2, self.resArchitecture: 2})


        self.lePalaisDAssur= Batiment("Le Palais d'Assur", 5, 14,
            {self.resPierre: 3, self.resBois: 1, self.resArchitecture: 4,
                                     self.resDecoration: 2})


        self.unChantier = Chantier(self.lePalaisDAssur)


    def testChantierEnvoyerTravaillerOuvrierInSuffisant(self):

        self.unChantier.envoyerTravaillerOuvrier(self.mano1)
        self.assertFalse(self.unChantier.envoyerTravaillerOuvrier(self.compa1), "Erreur équipe insuffisante")


    def testChantierEnvoyerTravaillerOuvrierSuffisant(self):

        self.unChantier.envoyerTravaillerOuvrier(self.mano1)
        self.unChantier.envoyerTravaillerOuvrier(self.compa1)
        self.assertTrue(self.unChantier.envoyerTravaillerOuvrier(self.ouv1), "Erreur équipe suffisante")



if __name__ == '__main__':
    unittest.main()
