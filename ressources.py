class Ressource:

    def __init__(self, label: str):

        self.setRessource(label)

    def getRessource(self) -> str:
        """
        getter le type de ressource
        :return: la rssource
        """
        return self.__label

    def setRessource(self, value: str) -> None:
        """
        setter le type de ressource
        :param value: string
        """

        self.__label = value

    def __str__(self):

        return self.__label